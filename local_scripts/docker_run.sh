source ../container-name.sh
IMAGE_NAME=$1

if [ $# -lt 2 ];
then
    echo "+ $0: Too few arguments!"
    echo "+ use something like:"
    echo "+ $0 reslocal/${CONTAINER_NAME} skope <commands>"
    echo "e.g.:"
    echo "+ $0 reslocal/${CONTAINER_NAME} skopeo inspect docker://docker.io/fedora"
    exit
fi

# remove currently running containers
echo "+ ID_TO_KILL=\$(docker ps -a -q  --filter ancestor=$1)"
ID_TO_KILL=$(docker ps -a -q  --filter ancestor=$1)

echo "+ docker ps -a"
docker ps -a
echo "+ docker stop ${ID_TO_KILL}"
docker stop ${ID_TO_KILL}
echo "+ docker rm -f ${ID_TO_KILL}"
docker rm -f ${ID_TO_KILL}
echo "+ docker ps -a"
docker ps -a

# -t : Allocate a pseudo-tty
# -i : Keep STDIN open even if not attached
# -d : To start a container in detached mode, you use -d=true or just -d option.
# -p : publish port PUBLIC_PORT:INTERNAL_PORT
# -l : ??? without it no root@1928719827
# --cap-drop=all: drop all (root) capabilites

echo "+ ID=\$(docker run -t -i $@)"
ID=$(docker run -t -i $@)

echo "+ ID ${ID}"

# let's attach to it:
#echo "+ docker attach ${ID}"
#docker attach ${ID}
